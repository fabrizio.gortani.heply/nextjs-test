import fetch from 'node-fetch'

// Please use dynamic import instead static one
// belowe
const API_URL = 'http://localhost/api'
const COOKIE_TOKEN_NAME = 'AUTH_TOKEN'
// import { COOKIE_TOKEN_NAME, API_URL } from '../app.config'

class API {
  constructor (baseURL = API_URL, token) {
    this.baseURL = baseURL
    this.token = token

    this.headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    }
  }

  setAuthToken (token = '') {
    this.token = token
  }

  setAuthTokenFromCookie (req, res) {
    const token = req.cookies[COOKIE_TOKEN_NAME]
    this.token = token
  }

  useAuthToken () {
    return {
      Authorization: `Bearer ${this.token}`
    }
  }

  /**
   * Ask API for csfr-cookie to use for perform POST,PATCH,DELETE
   * requests
   * @returns {Promise <array>}
   */
  async takeCSRF () {
    // Sactum CSFR token check is outside /api
    // scope. So we must simply rewrite the baseURL without api prefix.
    // In case of baseURL set as http://localhost/api we must obtain: http://localhost
    const apiURLWithoutAPIPrefix = this.baseURL.replace(/\/api/, '')

    let cookie
    try {
      const response = await this.request(
        `${apiURLWithoutAPIPrefix}/sanctum/csrf-cookie`,
        'GET',
        this.headers
      )
      cookie = response.headers.get('set-cookie')
    } catch (error) {
      return [null, error.message]
    }
    return [cookie, null]
  }

  /**
   * Transform object of queryParams to string. You can also
   * provide a blacklist for ignore certains parameters
   * @param {object} queryParams
   * @param {array} toIgnore
   * @returns { string }
   */
  toQueryString (queryParams = {}, toIgnore = []) {
    return Object.keys(queryParams)
      .filter(key => (toIgnore.length === 0 ? true : toIgnore.indexOf(key) < 0))
      .map(key => key + '=' + encodeURIComponent(queryParams[key]))
      .join('&')
  }

  /**
   * Generic request method for obtain data from API
   * @param {string} endpoint
   * @param {string} method
   * @param {object} headers
   * @param {object} payload
   * @returns {Promise}
   */
  async request (endpoint = '', method = 'GET', headers = {}, payload = {}) {
    return fetch(endpoint, {
      method,
      headers,
      ...payload
    })
  }

  async toResponse (response) {
    // If status code is not positive
    if (!response.ok) {
      let error = ''
      try {
        const data = await response.json()
        error = JSON.stringify(data)
      } catch (err) {
        throw new Error(err.message)
      }

      throw new Error(error)
    }

    // else
    return response.json()
  }

  /**
   * Path request
   * @param {string} endpoint
   * @param {object} body
   * @param {object} headers
   * @returns {Promise}
   */
  async patch (endpoint = '', body = {}, headers = {}) {
    const [cookie] = await this.takeCSRF()

    const aggregateHeaders = {
      ...this.headers,
      ...this.useAuthToken(),
      ...headers,
      'X-XSRF-TOKEN': cookie
    }

    // When you want to use multipart/form-data be sure to remove the Content-Type header
    // fetch automatically recognized the proper header. If you don't remove the content-type
    // the application fails
    if (headers['Content-Type'] === 'multipart/form-data') {
      delete aggregateHeaders['Content-Type']
    }

    const response = await this.request(
      this.baseURL + endpoint,
      'PATCH',
      aggregateHeaders,
      {
        body: JSON.stringify(body)
      }
    )

    return this.toResponse(response)
  }

  /**
   * Post request
   * @param {string} endpoint
   * @param {object} body
   * @param {object} headers
   * @returns {Promise}
   */
  async post (endpoint = '', body = {}, headers = {}) {
    const [cookie] = await this.takeCSRF()

    const aggregateHeaders = {
      ...this.headers,
      ...this.useAuthToken(),
      ...headers,
      'X-XSRF-TOKEN': cookie
    }

    const response = await this.request(
      this.baseURL + endpoint,
      'POST',
      aggregateHeaders,
      {
        body: JSON.stringify(body)
      }
    )

    return this.toResponse(response)
  }

  /**
   * Delete request
   * @param {string} endpoint
   * @param {object} body
   * @param {object} headers
   * @returns {Promise}
   */
  async delete (endpoint = '', body = {}, headers = {}) {
    const [cookie] = await this.takeCSRF()
    const response = await this.request(
      this.baseURL + endpoint,
      'DELETE',
      {
        ...this.headers,
        ...this.useAuthToken(),
        ...headers,
        'X-XSRF-TOKEN': cookie
      },
      { body: JSON.stringify(body) }
    )

    return this.toResponse(response)
  }

  /**
   * Get request
   * @param {string} endpoint
   * @param {object} queryParams
   * @param {object} headers
   * @returns {Promise}
   */
  async get (endpoint = '', queryParams = {}, headers = {}) {
    const url = this.baseURL + endpoint + '?' + this.toQueryString(queryParams)

    const headersAggregation = {
      ...this.headers,
      ...this.useAuthToken(),
      ...headers
    }

    const response = await this.request(url, 'GET', headersAggregation)

    return this.toResponse(response)
  }

  // List of endpoints to use here ...

  async login (email = '', password = '') {
    let data
    try {
      data = await this.post('/login', { email, password })
    } catch (error) {
      return [data, error.message]
    }

    return [data, null]
  }

  async getProfile () {
    let data
    try {
      data = await this.get('/profile')
    } catch (error) {
      return [data, error.message]
    }

    return [data, null]
  }
}

export default API
