// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import API from '../../helpers/api'

export default function handler (req, res) {
  const api = new API()

  // Use this method for set the auth token automatically
  // from cookie. Or you can also extract manually the token and use
  // the following method:
  // api.setAuthToken("your_token_here")
  api.setAuthTokenFromCookie(req, res)

  // Perform a request
  const [profile, errorProfile] = api.getProfile()
  if (errorProfile) {
    return res.stauts(500).json(errorProfile)
  }

  res.status(200).json({ profile })
}
